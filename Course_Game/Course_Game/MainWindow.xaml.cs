﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Threading;
using Timer = System.Windows.Forms.Timer;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using Point = System.Drawing.Point;

namespace Course_Game
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        
        Timer timer = new Timer();
        Timer timer2 = new Timer();
        Timer timerNewBrokkoli = new Timer();
        Timer timerWave = new Timer();


        public static List<Brokkoli_Class> brokkoliList = new List<Brokkoli_Class>();

        public static List<RedBrokkoli_Class> RedBrokkoliList = new List<RedBrokkoli_Class>();

        public static AmaneKanata_Class AmaneKanataPlayer;

        Random rand = new Random();

        //Для сравнения времени между первым и вторым кадром
        DateTime dtFirstFrame = DateTime.Now;
        DateTime dtLastFrame = DateTime.Now;

        static Bitmap bmp;
        //Отдельные изображения для многопоточности
        static Bitmap bmpBackGround;

        int BackGroundY;

        static System.Drawing.Image BackGroundImage = System.Drawing.Image.FromFile(@"game_resources\BackGround.png");

        static System.Drawing.Image Thunder = System.Drawing.Image.FromFile(@"game_resources\Thunder.png");

        static System.Drawing.Image GameoverImage = System.Drawing.Image.FromFile(@"game_resources\Gameover.png");

        static System.Drawing.Image Gameover2Image = System.Drawing.Image.FromFile(@"game_resources\GoodEnd.png");

        static System.Drawing.Image Studio = System.Drawing.Image.FromFile(@"game_resources\VasyanGames.png");

        static System.Drawing.Image Story = System.Drawing.Image.FromFile(@"game_resources\Story.png");

        Generator_Class generator;

        int maxGB = 0;

        bool WaveStart = false;

        bool RBisReady = false;

        private void GameWindow_Loaded(object sender, RoutedEventArgs e)
        {
            bmp = new Bitmap(Convert.ToInt32((int)myCanvas.ActualWidth), Convert.ToInt32((int)myCanvas.ActualHeight));
            var gfx = Graphics.FromImage(bmp);
            gfx.Clear(System.Drawing.Color.Transparent);
            gfx.DrawImage(Studio, new Point(0, 0));
            myImage2.Source = ImageSourceForBitmap(bmp);
 
        }

        void StartGame()
        {

            WaveStart = false;

            RBisReady = false;

            bmpBackGround = new Bitmap(Convert.ToInt32((int)myCanvas.ActualWidth), Convert.ToInt32((int)myCanvas.ActualHeight));

            timerWave.Tick += TimerWave_Tick;
            timerWave.Interval = 2000;

            timerNewBrokkoli.Tick += TimerNewBrokkoli_Tick;
            timerNewBrokkoli.Interval = 2000;

            timer2.Tick += Timer2_Tick;
            timer2.Interval = 1;


            timer.Tick += Timer_Tick;
            timer.Interval = 12;


            timer.Start();
            timer2.Start();
            timerNewBrokkoli.Start();

            maxGB = 50;

            generator = new Generator_Class(Convert.ToInt32((int)myCanvas.ActualWidth), Convert.ToInt32((int)myCanvas.ActualHeight));

            AmaneKanataPlayer = AmaneKanataGenerator();

            BackGroundY = BackGroundImage.Height - 800;
        }

        private void TimerWave_Tick(object? sender, EventArgs e)
        {
            brokkoliList.AddRange(generator.CrossLeftRight());
            if (brokkoliList.Count > maxGB + 40)
            {
                WaveStart = false;
                timerWave.Stop();
            }
                
        }

        private void TimerNewBrokkoli_Tick(object? sender, EventArgs e)
        {
            if (brokkoliList.Count < maxGB)
                NewGBAdd();
            if (RBisReady)
                NewRBAdd();
        }

        private void Timer2_Tick(object? sender, EventArgs e)
        {
            AmaneKanataPlayer.SetPosition(Convert.ToInt32(Mouse.GetPosition(System.Windows.Application.Current.MainWindow).X) - 40,
            Convert.ToInt32(Mouse.GetPosition(System.Windows.Application.Current.MainWindow).Y) - 30);

            
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            Render();
            
        }

        public AmaneKanata_Class AmaneKanataGenerator()
        {
            AmaneKanata_Class player = new AmaneKanata_Class(Convert.ToInt32(Mouse.GetPosition(System.Windows.Application.Current.MainWindow).X) - 40,
                Convert.ToInt32(Mouse.GetPosition(System.Windows.Application.Current.MainWindow).Y) - 30);
            player.Height = (int)myCanvas.ActualHeight;
            player.Width = (int)myCanvas.ActualWidth;
            return player;
        }

        



        public void Render()
        {
            
            var gfx = Graphics.FromImage(bmp);

            gfx.Clear(System.Drawing.Color.Transparent);

            //Работа с обычными Броккли
            foreach (var Brokkoli in brokkoliList)
            {
                Brokkoli.Execute();
                AmaneKanataPlayer.isIntersect(Brokkoli.x, Brokkoli.y, Brokkoli.image.Width, Brokkoli.image.Height);
                gfx.DrawImage(Brokkoli.image, Brokkoli.GetPoint());
            }

            //Работа с красными Броккли
            foreach (var Brokkoli in RedBrokkoliList)
            {
                Brokkoli.Execute();
                if (Brokkoli.CanBeDeleted)
                {
                    Brokkoli.Boom();
                    brokkoliList.AddRange(generator.gb4Boom(Brokkoli.x, Brokkoli.y));
                }
                    
                if (Brokkoli.xyStriving.X - Brokkoli.x < 4 & Brokkoli.xyStriving.Y - Brokkoli.y < 4)
                {
                    Brokkoli.CanBeDeleted = true;
                    Brokkoli.Boom();
                    brokkoliList.AddRange(generator.gb4Boom(Brokkoli.x, Brokkoli.y));
                }
                
                

                AmaneKanataPlayer.isIntersect(Brokkoli.x, Brokkoli.y, Brokkoli.image.Width, Brokkoli.image.Height);
                gfx.DrawImage(Brokkoli.image, Brokkoli.GetPoint());
            }

            

            //Отображение молнии
            if (AmaneKanataPlayer.CoolDown > 30)
                gfx.DrawImage(Thunder, new Point(0, 0));
            //Отрисовка игрока
            gfx.DrawImage(AmaneKanataPlayer.GetImage(), AmaneKanataPlayer.GetPoint());



            //Время, через секунду после dtFirstFrame для точного вычисления количества тиков за одну секунду (Оно постоянное)
            DateTime dateTime3 = dtFirstFrame.AddSeconds(1);

            //Техническая информация
            int fps = Convert.ToInt32((dateTime3 - dtFirstFrame).Ticks / (dtFirstFrame - dtLastFrame).Ticks);
            gfx.DrawString("FPS:" + fps.ToString() + " Items Count = " + (brokkoliList.Count() + RedBrokkoliList.Count() + 1).ToString() + " Interval:" + timer.Interval.ToString(),
                new Font("Arial", 16), new SolidBrush(System.Drawing.Color.White), new System.Drawing.Point(100,0));
            gfx.DrawString(AmaneKanataPlayer.Health.ToString(),
                new Font("Arial Black", 50), new SolidBrush(System.Drawing.Color.OrangeRed), new System.Drawing.Point(0, 0));

            //Вывод bmp на игровой экран
            myImage2.Source = ImageSourceForBitmap(bmp);
            dtLastFrame = dtFirstFrame;
            dtFirstFrame = DateTime.Now;

            gfx.Dispose();

            //Движение фона
            if (BackGroundY != 0)
            {
                BackGroundY--;
            }    
            else
                GameOver();

            //Вывод bmpBackGround на задний план игрового экрана
            var gfx2 = Graphics.FromImage(bmpBackGround);
            gfx2.DrawImage(BackGroundImage, new Point(0, -BackGroundY));
            myImage1.Source = ImageSourceForBitmap(bmpBackGround);

            //Сценарий уровня
            switch((100 - BackGroundY / 80))
            {
                case (75):
                    maxGB = 85;
                    timerNewBrokkoli.Interval = 1000;
                    break;
                case (50):
                    RBisReady = true;
                    maxGB = 50;
                    timerWave.Interval = 5000;
                    timerNewBrokkoli.Interval = 1250;
                    break;
                case (45):
                    maxGB = 70;
                    timerNewBrokkoli.Interval = 1000;
                    break;
                case (35):
                    maxGB = 125;
                    timerNewBrokkoli.Interval = 800;
                    break;

                case (25):
                    maxGB = 85;
                    timerWave.Interval = 4000;
                    timerNewBrokkoli.Interval = 1000;
                    break;
                case (15):
                    maxGB =  65;
                    timerNewBrokkoli.Interval = 1250;
                    break;
            }

            //Очистка разрешённых к удалению объектов
                ClearObjects();
            //Проверка на конец жизней
            if (AmaneKanataPlayer.Health == 0)
                GameOver();
        }

        /// <summary>
        /// Случайный выбор генератора зелёных брокколи
        /// </summary>
        void NewGBAdd()
        {
            switch (rand.Next(9))
            {
                case (0):
                    brokkoliList.AddRange(generator.gb5Left());
                    break;
                case (1):
                    brokkoliList.AddRange(generator.gb5Right());
                    break;
                case (2):
                    brokkoliList.AddRange(generator.UpDown5Right());
                    break;
                case (3):
                    brokkoliList.AddRange(generator.DownUp5Right());
                    break;
                case (4):
                    brokkoliList.AddRange(generator.gb3Left());
                    break;
                case (5):
                    brokkoliList.AddRange(generator.gb3Right());
                    break;
                case (6):
                    brokkoliList.AddRange(generator.UpDown3Right());
                    break;
                case (7):
                    brokkoliList.AddRange(generator.DownUp3Right());
                    break;
                case (8):
                    if (!WaveStart)
                    {
                        timerWave.Start();
                    }
                    else
                        timerWave.Stop();
                    break;
                

            }
                
        }
        /// <summary>
        /// Случайный выбор генератора красных брокколи
        /// </summary>
        void NewRBAdd()
        {
            switch (rand.Next(4))
            {
                case (0):
                    RedBrokkoliList.AddRange(generator.RedSpeedBrokkoliLeft(AmaneKanataPlayer.x, AmaneKanataPlayer.y));
                    break;
                case (1):
                    RedBrokkoliList.AddRange(generator.RedSpeedBrokkoliRigth(AmaneKanataPlayer.x, AmaneKanataPlayer.y));
                    break;
            }
        }


        //Почистить списки
        void ClearObjects()
        {
            brokkoliList.RemoveAll(d => d.CanBeDeleted);
            RedBrokkoliList.RemoveAll(d => d.CanBeDeleted);
        }
        
        public void GameOver()
        {

            var gfx = Graphics.FromImage(bmp);
            gfx.Clear(System.Drawing.Color.Transparent);

            if (AmaneKanataPlayer.Health == 0)
                gfx.DrawImage(GameoverImage, new Point(0, 0));
            else
                gfx.DrawImage(Gameover2Image, new Point(0, 0));

            myImage2.Source = ImageSourceForBitmap(bmp);

            timer.Stop();
            timer2.Stop();
            timerNewBrokkoli.Stop();
            
            timerWave.Stop();
            brokkoliList.Clear();
            RedBrokkoliList.Clear();

            

            StartButton.Visibility = Visibility.Visible;
        }


        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject([In] IntPtr hObject);

        public ImageSource ImageSourceForBitmap(Bitmap bmp)
        {
            var handle = bmp.GetHbitmap();
            try
            {
                return Imaging.CreateBitmapSourceFromHBitmap(handle, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
            finally { DeleteObject(handle); }
        }



        private void myCanvas_Loaded(object sender, RoutedEventArgs e) => Render();
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e) => Render();
        private void myCanvas_MouseDown(object sender, MouseButtonEventArgs e) => Render();


        private void GameWindow_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            
        }

        int ButtonClicked = 0;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ButtonClicked++;
            switch (ButtonClicked)
            {
                case 1:
                    bmp = new Bitmap(Convert.ToInt32((int)myCanvas.ActualWidth), Convert.ToInt32((int)myCanvas.ActualHeight));
                    var gfx = Graphics.FromImage(bmp);
                    gfx.Clear(System.Drawing.Color.Transparent);
                    gfx.DrawImage(Story, new Point(0, 0));
                    myImage2.Source = ImageSourceForBitmap(bmp);
                    StartButton.Content = "Start game!";
                    break;
                default:
                    StartGame();
                    StartButton.Visibility = Visibility.Hidden;
                    break;
            }
        }
    }
}

