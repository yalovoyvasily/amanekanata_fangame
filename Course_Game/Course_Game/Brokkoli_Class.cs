﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace Course_Game
{
    public class Brokkoli_Class
    {
        public int x { get; set; }
        public int y { get; set; }

        public int Height { get; set; }
        public int Width { get; set; }

        //Точка стремления, если чо
        public Point xyStriving { get; set; }

        public int Speed = 1;

        public bool CanBeDeleted = false;

        public Image image = Image.FromFile(@"game_resources\Small1.png");
       //public Image image = Image.FromFile(@"game_resources\Point.png");


        Random rand = new Random();



        public int PointDistance { get; set; }

        public int Parts { get; set; }

        public int LenPart { get; set; }

        public int CurLenPart { get; set; }

        
        public Brokkoli_Class(int _x = 0, int _y = 0, int _xStriving = 0, int _yStriving = 0)
        {
            x = _x;
            y = _y;
            if (x > 0 && y > 0 && _xStriving > 0 && _yStriving > 0)
                SetStrivingPoint(new Point(_xStriving, _yStriving));
        }

        
        public void Execute()
        {

            int xLength = Math.Abs(xyStriving.X - x);
            int yLength = Math.Abs(xyStriving.Y - y);


            if (xLength > yLength)
            {
                if (x != xyStriving.X && CurLenPart < LenPart)
                {
                    CurLenPart++;
                    if (x < xyStriving.X)
                        x += Speed;
                    if (x > xyStriving.X)
                        x -= Speed;
                }

                if (CurLenPart == LenPart)
                {
                    CurLenPart = 0;
                    if (y < xyStriving.Y)
                        y += Speed;
                    if (y > xyStriving.Y)
                        y -= Speed;
                }

                if (yLength == 0 || xLength == 0)
                {
                    if (x < xyStriving.X)
                        x += Speed;
                    if (x > xyStriving.X)
                        x -= Speed;

                    if (y < xyStriving.Y)
                        y += Speed;
                    if (y > xyStriving.Y)
                        y -= Speed;
                }

            }
            else
            {
                if (yLength > xLength)
                {
                    if (y != xyStriving.Y && CurLenPart < LenPart)
                    {
                        CurLenPart++;
                        if (y < xyStriving.Y)
                            y += Speed;
                        if (y > xyStriving.Y)
                            y -= Speed;
                    }

                    if (CurLenPart == LenPart)
                    {
                        CurLenPart = 0;
                        if (x < xyStriving.X)
                            x += Speed;
                        if (x > xyStriving.X)
                            x -= Speed;
                    }

                    
                    if (yLength == 0 || xLength == 0)
                    {
                        if (x < xyStriving.X)
                            x += Speed;
                        if (x > xyStriving.X)
                            x -= Speed;

                        if (y < xyStriving.Y)
                            y += Speed;
                        if (y > xyStriving.Y)
                            y -= Speed;
                    }


                }
                else
                {
                    if (xLength == yLength)
                    {
                        if (x < xyStriving.X)
                            x += Speed;
                        if (x > xyStriving.X)
                            x -= Speed;

                        if (y < xyStriving.Y)
                            y += Speed;
                        if (y > xyStriving.Y)
                            y -= Speed;
                    }
                    
                }

            }


            if (xyStriving.X == x && xyStriving.Y == y)
                CanBeDeleted = true;
        }

        public void SetStrivingPoint(Point point)
        {
            xyStriving = point;

            double xLength = xyStriving.X - x;
            double yLength = xyStriving.Y - y;

            PointDistance = Convert.ToInt32(Math.Sqrt(Math.Pow(xyStriving.X - x, 2) + Math.Pow(xyStriving.Y - y, 2)));

            double NumAdjust = 0.4;

            if (Math.Abs(xLength) > Math.Abs(yLength) && yLength != 0)
            {
                Parts = Math.Abs(Convert.ToInt32(yLength));
                double temp = Math.Abs(xLength) / Convert.ToDouble(Parts) + NumAdjust;
                LenPart = Math.Abs(Convert.ToInt32(Math.Round(temp)));

                if (yLength / (xLength / 100) > 60 && yLength > 100)
                    LenPart++;
            }
                
            else
                if (Math.Abs(yLength) > Math.Abs(xLength) && xLength != 0)
            {
                Parts = Math.Abs(Convert.ToInt32(xLength));
                double temp = Math.Abs(yLength) / Convert.ToDouble(Parts) + NumAdjust;
                LenPart = Math.Abs(Convert.ToInt32(Math.Round(temp)));

                if (xLength / (yLength / 100) > 60 && xLength > 100)
                    LenPart++;
            }
                
            else
                if (xLength == yLength)
                Parts = Parts = Math.Abs(Convert.ToInt32(xLength));
            

            if (Parts == 0)
                Parts = 2;


            CurLenPart = 0;
        }

        public Point GetPoint()
        {
            return new Point(x, y);
        }

    }

    public class RedBrokkoli_Class : Brokkoli_Class
    {
        
        public Image image = Image.FromFile(@"game_resources\Brokkoli2.png");
        private Image boomImage = Image.FromFile(@"game_resources\Boom.png");

        public RedBrokkoli_Class(int _x = 0, int _y = 0, int _xStriving = 0, int _yStriving = 0)
        {
            Speed = 4;
            x = _x;
            y = _y;
            SetStrivingPoint(new Point(_xStriving, _yStriving));
        }

        public void Boom()
        {
            image = boomImage;
        }
    }

    public class AmaneKanata_Class : Brokkoli_Class
    {
        public Image Image1 = Image.FromFile(@"game_resources\Kanata1.png");
        public Image Image2 = Image.FromFile(@"game_resources\Kanata2.png");
        public Image Image3 = Image.FromFile(@"game_resources\Kanata3.png");
        public Image Image4 = Image.FromFile(@"game_resources\Kanata4.png");

        private int Frame = 0;

        private int FrameSkip = 0;

        public int Health = 5;

        public int CoolDown = 0;
        public AmaneKanata_Class(int _x = 0, int _y = 0)
        {
            x = _x;
            y = _y;
            
            //SetStrivingPoint(new Point(_xStriving, _yStriving));
        }

        //Пропуск кадров для возможности увидеть анимацию человеческим глазом
        public Image GetImage()
        {
            if (CoolDown != 0)
            {
                CoolDown--;
                return Image1;
            }

            if (FrameSkip < 4)
            {
                FrameSkip++;
                switch (Frame)
                {
                    case 1:
                        return Image1;
                    case 2:
                        return Image2;
                    case 3:
                        return Image3;
                    case 4:
                        return Image4;
                }
            }
                
            else
                FrameSkip = 1;

            if (Frame < 4)
                Frame++;
            else
                Frame = 1;


            switch (Frame)
            {
                case 1:
                    return Image1;
                case 2:
                    return Image2;
                case 3:
                    return Image3;
                case 4:
                    return Image4;
            }
                
            return Image1;
        }

        /// <summary>
        /// Соприкасается ли объект с игроком
        /// </summary>
        /// <param name="figureX"></param>
        /// <param name="figureY"></param>
        /// <param name="figureWidth"></param>
        /// <param name="figureHeight"></param>
        public void isIntersect(int figureX, int figureY, int figureWidth, int figureHeight)
        {
            int figureX2 = figureX + figureWidth;
            int figureY2 = figureY + figureHeight;

            int Deviation = 20;

            int X = x + Deviation;
            int Y = y + Deviation;

            int X2 = x + Image1.Width - Deviation;
            int Y2 = y + Image1.Height - Deviation;

            if (!(figureX > X2 || X > figureX2 || figureY2 < Y || figureY > Y2) && CoolDown == 0)
            {
                
                EatBrokkoli();
                
            }

           
            
        }

        public void SetPosition(int _x, int _y)
        {
            if (_x < Width && _y < Height && _x > -1 && _y > -1)
            {
                x = _x;
                y = _y;
            }
        }

        public void EatBrokkoli()
        {
            CoolDown = 60;
            if (Health != 0)
                Health--;
        }
    }
}
