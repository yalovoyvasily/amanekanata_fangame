﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course_Game
{
    public class Generator_Class
    {
        public int Height;
        public int Width;
        public Generator_Class(int _Height, int _Width)
        {
            Height = _Height;
            Width = _Width;
        }

        Random rand = new Random();

        public List<Brokkoli_Class> gb5Left()
        {
            List <Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 6; i++)
                list.Add(new Brokkoli_Class(1, Height / 6 * i,
                    Width + 10, Height / 6 * i));
            return list;
        }

        public List<Brokkoli_Class> gb5Right()
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 6; i++)
                list.Add(new Brokkoli_Class(Width + 10, Height / 6 * i,
                   1 , Height / 6 * i));
            return list;
        }

        public List<Brokkoli_Class> UpDown5Right()
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 6; i++)
                list.Add(new Brokkoli_Class(Width / 6 * i , 1,
                   Width / 6 * i, Height));
            return list;
        }

        public List<Brokkoli_Class> DownUp5Right()
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 6; i++)
                list.Add(new Brokkoli_Class(Width / 6 * i, Height,
                   Width / 6 * i, 1));
            return list;
        }

        public List<Brokkoli_Class> gb3Left()
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 4; i++)
                list.Add(new Brokkoli_Class(1, Height / 4 * i,
                    Width + 10, Height / 4 * i));
            return list;
        }

        public List<Brokkoli_Class> gb3Right()
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 4; i++)
                list.Add(new Brokkoli_Class(Width + 10, Height / 4 * i,
                   1, Height / 4 * i));
            return list;
        }

        public List<Brokkoli_Class> UpDown3Right()
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 4; i++)
                list.Add(new Brokkoli_Class(Width / 4 * i, 1,
                   Width / 4 * i, Height));
            return list;
        }

        public List<Brokkoli_Class> DownUp3Right()
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 4; i++)
                list.Add(new Brokkoli_Class(Width / 4 * i, Height,
                   Width / 4 * i, 1));
            return list;
        }

        public List<Brokkoli_Class> CrossLeftRight()
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            for (int i = 1; i < 4; i++)
                list.Add(new Brokkoli_Class(10 * i, 1,
                   Width * i, Height));
            return list;
        }

        public List<RedBrokkoli_Class> RedSpeedBrokkoliLeft(int _x, int _y)
        {
            List<RedBrokkoli_Class> list = new List<RedBrokkoli_Class>();
            list.Add(new RedBrokkoli_Class(1, 1,
                  _x, _y));
            return list;
        }

        public List<RedBrokkoli_Class> RedSpeedBrokkoliRigth(int _x, int _y)
        {

            List<RedBrokkoli_Class> list = new List<RedBrokkoli_Class>();
            list.Add(new RedBrokkoli_Class(Width, 1,
                  _x, _y));
            return list;
        }

        public List<Brokkoli_Class> gb4Boom(int _x, int _y)
        {
            List<Brokkoli_Class> list = new List<Brokkoli_Class>();
            list.Add(new Brokkoli_Class(_x, _y, 1, 1));
            list.Add(new Brokkoli_Class(_x, _y, Width, 1));
            list.Add(new Brokkoli_Class(_x, _y, 1, Height));
            list.Add(new Brokkoli_Class(_x, _y, Width, Height));
            return list;
        }

    }
}
